﻿using Amazon;

namespace SnsTest.Droid
{
    public class Constants
    {
        //identity pool id for cognito credentials
        public const string IdentityPoolId = "us-east-1:b3d38307-0048-4cbb-b057-7b660dc4f233";

        //sns android platform arn
        public const string AndroidPlatformApplicationArn = "arn:aws:sns:us-east-1:929212090450:app/GCM/SnsTest";

        //project id for android gcm
        public const string GoogleConsoleProjectId = "198436824910";

        public static RegionEndpoint CognitoRegion = RegionEndpoint.USEast1;
        public static RegionEndpoint SnsRegion = RegionEndpoint.USEast1;

    }
}